package com.vijay.SpringBootProjectWithSecurity.utils;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import springfox.documentation.spring.web.json.Json;

/**
 * This class is important since when we use gson for our serlization and deserilialisation we get an error when the spring info is converted to json
 * It looks like below. the below value is from Class Json in springfox
 * {
*"value": "{\"swagger\":\"2.0\",\"info\":{\"description\":\"Api Documentation\",\"version\":\"1.0\",\"title\":\"Api Documentation\",\"termsOfService\":\"urn:tos\",\"contact\":{\"name\":\"Contact Email\"},\"license\":{\"name\":\"Apache 2.0\",\"url\":\"http:
*...
*It should never be the case. It should be a json not a jsonString
 * @author vijayakumar_psg587
 *
 */
public class SpringFoxGsonAdapter implements JsonDeserializer<Json>{

	@Override
	public Json deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		
		final JsonObject jObj = json.getAsJsonObject();
		return new Json(
				jObj.get("value").getAsString());
		// TODO Auto-generated method stub
		
		
		
	}
	
	
	
//implements  TypeAdapterFactory {
//
//	@Override
//	public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
//		// TODO Auto-generated method stub
//		if(!Json.class.isAssignableFrom(type.getRawType())) {
//			return null;
//		}
//		return (TypeAdapter<T>) new JsonAdapter();
//	}
//
//	private class JsonAdapter extends TypeAdapter<Json>{
//
//		@Override
//		public void write(JsonWriter out, Json jObjFromSpringFox) throws IOException {
//			
//			out.beginObject();
//			out.name("value").value(jObjFromSpringFox.value());
//			
//		}
//
//		@Override
//		public Json read(JsonReader in) throws IOException {
//			// TODO Auto-generated method stub
//			Json jObjFromSpringFox = null;
//			in.beginObject();
//			while(in.hasNext()) {
//				jObjFromSpringFox	= new Json(in.nextString());
//			}
//			return jObjFromSpringFox;
//		}
//		
//	}
	
	
}
