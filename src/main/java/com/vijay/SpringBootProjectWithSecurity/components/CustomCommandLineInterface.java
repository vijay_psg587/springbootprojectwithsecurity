package com.vijay.SpringBootProjectWithSecurity.components;

import org.springframework.beans.BeansException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.stereotype.Component;

@Component
public class CustomCommandLineInterface implements CommandLineRunner, ApplicationContextAware{
	//by now context would be available so when you implements contextAware
	//context obj is injected
	
	private ApplicationContext context;
	
	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("Inside run method"+this.context);
		ConfigurableEnvironment env = (ConfigurableEnvironment) this.context.getEnvironment();
		env.setActiveProfiles("QA");
		System.out.println("After setting profiles:"+ env);
	}

	public void setApplicationContext(ApplicationContext arg0) throws BeansException {
		// TODO Auto-generated method stub
		this.context = arg0;
	}

	
}
