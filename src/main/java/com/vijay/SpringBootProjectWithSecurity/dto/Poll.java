package com.vijay.SpringBootProjectWithSecurity.dto;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Poll implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SerializedName(value="Id")
	@Expose
	private int id;
	
	@SerializedName(value="Poll_Name")
	@Expose
	private String poll_name;
	
	@SerializedName(value="Poll_Count")
	@Expose
	private int poll_count;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPoll_name() {
		return poll_name;
	}
	public void setPoll_name(String poll_name) {
		this.poll_name = poll_name;
	}
	public int getPoll_count() {
		return poll_count;
	}
	public void setPoll_count(int poll_count) {
		this.poll_count = poll_count;
	}
	
}
