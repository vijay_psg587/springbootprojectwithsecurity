package com.vijay.SpringBootProjectWithSecurity.dto;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PollList  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SerializedName("Polls")
	@Expose
	private List<Poll> pollList;

	public List<Poll> getPollList() {
		return pollList;
	}

	public void setPollList(List<Poll> pollList) {
		this.pollList = pollList;
	}
	
	

}
